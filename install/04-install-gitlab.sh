#!/bin/bash

echo "export GITLAB_DIR=/storage/docker-homol/deploy/gitlab" >> $HOME/.bashrc

sudo mkdir -p /storage/docker-homol/deploy/gitlab/{data,logs,config}

docker run -dit \
  -p "2222:22" \
  -p "80:80" \
  -p "443:443" \
  --name gitlab \
  --restart always \
  --hostname "35.226.85.193" \
  -v /storage/docker-homol/deploy/gitlab/data:/var/opt/gitlab \
  -v /storage/docker-homol/deploy/gitlab/logs:/var/log/gitlab \
  -v /storage/docker-homol/deploy/gitlab/config:/etc/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ce:latest

# REFERENCIA: https://docs.gitlab.com/ee/install/docker.html

# CREATE A gitlab.rb file

echo "external_url 'http://35.226.85.193/'" >> gitlab.rb
